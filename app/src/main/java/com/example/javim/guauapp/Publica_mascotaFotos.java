package com.example.javim.guauapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.media.Image;

import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import android.os.PersistableBundle;
import android.provider.MediaStore;

import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


import java.io.ByteArrayOutputStream;
import java.io.File;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Build.VERSION_CODES.M;



public class Publica_mascotaFotos extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE=1;
    private String APP_DIRECTORY="MyPictureApp/";
    private String MEDIA_DIRECTORY = APP_DIRECTORY+"GuauApp";

    private final int MY_PERMISSION=100;
    private final int PHOTO_CODE=200;
    private final int SELECT_PICTURE=300;



    int band =1;
    Bitmap Bimg1;
    Base64 img_64;
    String urlUpload ="https://guau.000webhostapp.com/upload.php";

    Bitmap newBitmap;
    TextView msgErr;
    ImageButton btnTakePh;
    String path;
    Uri file;


    ScrollView mRlView;

    String mPath;
    public boolean mayRequestStoragePermission() {
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M )
                return true;

            if((checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                    (checkSelfPermission(CAMERA)== PackageManager.PERMISSION_GRANTED))
                return true;

            if((shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE))||
                    (shouldShowRequestPermissionRationale(CAMERA))) {
                Snackbar.make(mRlView, "Los permisos son necesarios para poder usar la aplicacion",
                        Snackbar.LENGTH_INDEFINITE).setAction(android.R.string.ok, new View.OnClickListener() {
                    @RequiresApi(api = M)
                    @Override
                    public void onClick(View v) {
                        requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA},1);
                    }
                }).show();
                //return true;
            }
            else
                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA},1);

            return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode==1){
            if(grantResults.length==2 && grantResults[0] ==PackageManager.PERMISSION_GRANTED &&
                    grantResults[1]==PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "Permisos aceptados", Toast.LENGTH_SHORT).show();
                btnTakePh.setEnabled(true);
            }
        }
        else
            showExplanation();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
       // super.onSaveInstanceState(outState, outPersistentState);
        outState.putString("path", mPath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mPath = savedInstanceState.getString("path");

    }

    private void openCamera(){
        File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);

        boolean isDirectoryCreated=file.exists();

        if(!isDirectoryCreated)
           isDirectoryCreated= file.mkdirs();

        if(isDirectoryCreated){
            Long timestamp = System.currentTimeMillis()/1000;
            String imageName = timestamp.toString()+".png";
             mPath= Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY + File.separator + imageName;
             File newFile = new File(mPath);
             Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(newFile));
            startActivityForResult(intent, PHOTO_CODE);
        }
    }



    private void showExplanation() {
        AlertDialog.Builder  builder = new AlertDialog.Builder(Publica_mascotaFotos.this);
        builder.setTitle("Permisos denegados");
        builder.setMessage("Para usar las funciones de Guau necesitas aceptar los persmisos");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent();
                i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                i.setData(uri);
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publica_mascota_fotos);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

         btnTakePh = (ImageButton) findViewById(R.id.btnTakePh);
        if(mayRequestStoragePermission())
            btnTakePh.setEnabled(true);
        else
            btnTakePh.setEnabled(false);


        final ImageView img1 = (ImageView) findViewById(R.id.img1);
        msgErr = (TextView) findViewById(R.id.msgErr);
        mRlView = (ScrollView)findViewById(R.id.mRlView);
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img1.setImageDrawable(null);
                band = 1;
            }
        });

        btnTakePh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(band ==1){
                   // llamarIntent();
                    showOptions();
                }
                else
                    Toast.makeText(Publica_mascotaFotos.this, "Aun no has tomado ninguna foto", Toast.LENGTH_SHORT).show();
            }
        });

        Button btnfinish = (Button)findViewById(R.id.finish);
        btnfinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(Publica_mascotaFotos.this, "Fotos subidas", Toast.LENGTH_SHORT).show();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, urlUpload, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                        msgErr.setText("Error:"+response);
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            if(respuestaJSON.getString("status").equals("1")){
                                final Intent i2 = new Intent(Publica_mascotaFotos.this, MainActivity.class);
                                startActivity(i2);
                            }
                            else
                                Toast.makeText(Publica_mascotaFotos.this,"Error, por intenta nuevamente", Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                        msgErr.setText("Error:"+error);
                    }
                    //mg1=transform(imageBitmap);
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        newBitmap = BitmapFactory.decodeFile(mPath);
                        String imageData = imageToString(newBitmap);
                        params.put("image", imageData);
                        params.put("id_usuario","1");//Aqui se cambia el id de usuario por el traido de login
                        return params;
                    }
                };
                stringRequest.setShouldCache(false);
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                                20000,//time to wait for it in this case 20s
                                20,//tryies in case of error
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
                RequestQueue requestQueue = Volley.newRequestQueue(Publica_mascotaFotos.this);
                requestQueue.add(stringRequest);


            }
        });
    }

    private void showOptions(){
        final CharSequence[] option = {"Tomar foto", "Elegir de galeria", "Cancelar"};

        final AlertDialog.Builder builder = new AlertDialog.Builder(Publica_mascotaFotos.this);
        builder.setTitle("Elige una opcion");
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(option[which]=="Tomar foto"){
                    openCamera();
                }else if(option[which]=="Elegir de galeria"){
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    intent.setType("images/*");
                    startActivityForResult(Intent.createChooser(intent, "Selecciona App de imagen"), SELECT_PICTURE );
                }
                else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }



   @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
       ImageView mSetImage = findViewById(R.id.img1);
        if(resultCode == RESULT_OK ){
            switch (requestCode){
                case PHOTO_CODE:
                    MediaScannerConnection.scanFile(this,
                            new String[]{mPath}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.i("ExternalStorage", "Scanned"+path+":");
                                    Log.i("ExternalStorage", "Uri:"+uri);
                                }
                            });
                    Bitmap bitmap = BitmapFactory.decodeFile(mPath);

                    mSetImage.setImageBitmap(bitmap);
                    break;
                case SELECT_PICTURE:
                    Uri path = data.getData();
                    mSetImage.setImageURI(path);
                    break;
            }
    }
    else
        Toast.makeText(getApplicationContext(),"Data vacio ",Toast.LENGTH_LONG).show();
    }

    private String imageToString(Bitmap bitmap){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize=1;

        bitmap.compress(Bitmap.CompressFormat.PNG, 50, outputStream);
        byte[] imageBytes = outputStream.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }
}
