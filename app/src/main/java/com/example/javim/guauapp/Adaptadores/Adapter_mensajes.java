package com.example.javim.guauapp.Adaptadores;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.javim.guauapp.R;

import java.util.ArrayList;

/**
 * Created by javim on 01/11/2017.
 */

public class Adapter_mensajes extends BaseAdapter {


    private ArrayList<items_mensajes> arrayListMensajes;
    private Context context;
    private LayoutInflater layoutInflater;

    public Adapter_mensajes(ArrayList<items_mensajes> arrayListMensajes, Context context){
        this.arrayListMensajes = arrayListMensajes;
        this.context = context;
    }

    @Override
    public int getCount() {return arrayListMensajes.size();}

    @Override
    public Object getItem(int i) {return arrayListMensajes.get(i);}

    @Override
    public long getItemId(int i) {return i;}

    @Override
    public View getView(int i, View view, final ViewGroup viewGroup) {
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View vistaItem = layoutInflater.inflate(R.layout.mensajes_layout, viewGroup,false);

        TextView id_persona = vistaItem.findViewById(R.id.id_persona);
        ImageView imgFotoPersona = vistaItem.findViewById(R.id.imgFotoPersona);
        TextView mensaje = vistaItem.findViewById(R.id.mensaje);

        id_persona.setText(arrayListMensajes.get(i).getId());
        imgFotoPersona.setImageBitmap(arrayListMensajes.get(i).getFoto());
        mensaje.setText(arrayListMensajes.get(i).getMensaje());

        mensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(viewGroup.getContext(), "Hola", Toast.LENGTH_SHORT).show();
            }
        });
        return vistaItem;
    }
}
