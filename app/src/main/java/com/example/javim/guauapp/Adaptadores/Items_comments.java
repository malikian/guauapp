package com.example.javim.guauapp.Adaptadores;

/**
 * Created by javim on 08/02/2018.
 */

public class Items_comments {

    public Items_comments(int id_comentario, int id_usuario, int id_mascota, String comentarios, String fecha_comentario, String nombreUsuario) {
        this.id_comentario = id_comentario;
        this.id_usuario = id_usuario;
        this.id_mascota = id_mascota;
        this.comentarios = comentarios;
        this.fecha_comentario = fecha_comentario;
        this.nombreUsuario = nombreUsuario;
    }

    private int id_comentario;
    private int id_usuario;
    private int id_mascota;
    private String nombreUsuario;
    private String comentarios;
    private String fecha_comentario;

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public int getId_comentario() {
        return id_comentario;
    }

    public void setId_comentario(int id_comentario) {
        this.id_comentario = id_comentario;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_mascota() {
        return id_mascota;
    }

    public void setId_mascota(int id_mascota) {
        this.id_mascota = id_mascota;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getFecha_comentario() {
        return fecha_comentario;
    }

    public void setFecha_comentario(String fecha_comentario) {
        this.fecha_comentario = fecha_comentario;
    }


}
