package com.example.javim.guauapp.Adaptadores;

/**
 * Created by javim on 14/01/2018.
 */

public class Items_mascotas {
    private int id_mascota;
    private int id_usuario;
    private String nombre_adoptante;
    private String nombre_mascota;
    private String especie;
    private String Raza;
    private int edad;
    private int mes_anio;
    private String descripcion;
    private int likes;
    private String tamanio;
    private String fecha_public;
    private int Status;
    private String link_image;
    private int interesados;
    private int liked;
    private int interesI;
    private String telefono;




    public Items_mascotas(int id_mascota, int id_usuario, String nombre_mascota,
                          String especie, String Raza, int edad, int mes_anio,
                          String descripcion, int likes, int interesados , String tamanio,
                          String fecha_public, int Status, String link_image, int liked,
                          int interesI, String nombre_adoptante, String telefono ){

        this.id_mascota = id_mascota;
        this.id_usuario = id_usuario;
        this.nombre_mascota = nombre_mascota;
        this.especie = especie;
        this.Raza = Raza;
        this.edad = edad;
        this.mes_anio = mes_anio;
        this.descripcion = descripcion;
        this.likes = likes;
        this.tamanio = tamanio;
        this.fecha_public = fecha_public;
        this.Status = Status;
        this.link_image = link_image;
        this.interesados = interesados;
        this.liked = liked;
        this.interesI = interesI;
        this.nombre_adoptante = nombre_adoptante;
        this.telefono = telefono;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getNombre_adoptante() {return nombre_adoptante;}

    public void setNombre_adoptante(String nombre_adoptante) {this.nombre_adoptante = nombre_adoptante;}

    public int getInteresI() {return interesI;}

    public void setInteresI(int interesI) {this.interesI = interesI;}

    public int getLiked() {return liked;}

    public void setLiked(int liked) {this.liked = liked;}

    public int getInteresados() {
        return interesados;
    }

    public void setInteresados(int interesados) {
        this.interesados = interesados;
    }

    public String getLink_image() {
        return link_image;
    }

    public void setLink_image(String link_image) {
        this.link_image = link_image;
    }

    public int getId_mascota() {
        return id_mascota;
    }

    public void setId_mascota(int id_mascota) {
        this.id_mascota = id_mascota;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre_mascota() {
        return nombre_mascota;
    }

    public void setNombre_mascota(String nombre_mascota) {
        this.nombre_mascota = nombre_mascota;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getRaza() {
        return Raza;
    }

    public void setRaza(String raza) {
        Raza = raza;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getMes_anio() {
        return mes_anio;
    }

    public void setMes_anio(int mes_anio) {
        this.mes_anio = mes_anio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getTamanio() {
        return tamanio;
    }

    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }

    public String getFecha_public() {
        return fecha_public;
    }

    public void setFecha_public(String fecha_public) {
        this.fecha_public = fecha_public;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }






}
