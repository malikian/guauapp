package com.example.javim.guauapp.Adaptadores;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.javim.guauapp.Clases.ObtenerWebService;
import com.example.javim.guauapp.Comentarios_publ;
import com.example.javim.guauapp.Publica_mascotaFotos;
import com.example.javim.guauapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by javim on 31/10/2017.
 */

public class MascotaAdapter extends RecyclerView.Adapter<MascotaAdapter.ViewHolder> {

    private Context context;
    private int layout;
    private ArrayList<Items_mascotas> arrayListMascotas;

    private TextView nombreMascota;
    private TextView nombrePersona;
    private TextView Descripcion;
    private TextView id_mascota;
    private TextView likes;
    private TextView interesados;
    private ImageButton imbComm;
    private ImageButton imbt1;
    private ImageButton imbInteres;
    private TextView tVContactar;
    private ImageButton btnMenu;


    public MascotaAdapter(Context context, ArrayList<Items_mascotas> arrayListMascotas, int layout) {
        this.context = context;
        this.arrayListMascotas = arrayListMascotas;
        this.layout = layout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(context).inflate(layout, null, false);
        imbt1 = v.findViewById(R.id.btnMegusta);
        this.imbComm = v.findViewById(R.id.btnMensaje);
        nombrePersona = v.findViewById(R.id.nombrePersona);
        nombreMascota = v.findViewById(R.id.nombreMascota);
        Descripcion = v.findViewById(R.id.descripcion);
        likes = v.findViewById(R.id.textoMeGusta);
        interesados = v.findViewById(R.id.textoMeInteresa);
        id_mascota = v.findViewById(R.id.id_mascota);
        imbInteres = v.findViewById(R.id.btnMeInteresa);
        tVContactar = v.findViewById(R.id.tVContactar);
        btnMenu = v.findViewById(R.id.btnMenu);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Picasso.with(context)
                .load(arrayListMascotas.get(position).getLink_image()).fit()
                .placeholder(R.drawable.ic_camera_black_24dp)
                .centerCrop()
                .into(holder.image, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                        //Toast.makeText(context, "Error! Revisa tu conexión a internet y vuelve a intentarlo", Toast.LENGTH_LONG).show();
                    }
                });

        id_mascota.setText(arrayListMascotas.get(position).getId_mascota() + "");
        nombrePersona.setText(arrayListMascotas.get(position).getNombre_adoptante());
        nombreMascota.setText(arrayListMascotas.get(position).getNombre_mascota());
        Descripcion.setText(arrayListMascotas.get(position).getDescripcion());
        likes.setText("A " + arrayListMascotas.get(position).getLikes() + " les gusta");
        interesados.setText("a " + arrayListMascotas.get(position).getInteresados() + " les interesa");
        if (arrayListMascotas.get(position).getLiked() == 1) {
            imbt1.setImageResource(R.drawable.ic_favorite_black_24dp);
            imbt1.setTag("rojo");
        } else
            imbt1.setTag("negro");

        if (arrayListMascotas.get(position).getInteresI() == 1) {
            imbInteres.setImageResource(R.drawable.ic_sentiment_very_satisfied_red_24dp);
            imbInteres.setTag("rojo");
        } else
            imbInteres.setTag("negro");

        //imbComm = view.findViewById(R.id.btnMensaje);

        tVContactar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // CallIntentCall(arrayListMascotas.get(position).getTelefono());
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    NewerVersion();
                }
                else{
                    OlderVersions();
                }
            }

            private void OlderVersions() {
                Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + arrayListMascotas.get(position).getTelefono()));
                if (CheckPermission(Manifest.permission.CALL_PHONE)) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    context.startActivity(i);
                }

            }
            private void NewerVersion(){

            }

        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptions();
            }
        });
    }
    private void showOptions(){
        final CharSequence[] option = {"Reportar", "Copiar enlace","Contactar ", "Cancelar"};

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Elige una opcion");
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(option[which]=="Reportar"){

                }else if(option[which]=="Copiar enlace"){

                }else if(option[which]=="Contactar"){

            }
                else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private boolean CheckPermission(String permission){
        int result = context.checkCallingOrSelfPermission(permission);
        return result== PackageManager.PERMISSION_GRANTED;
    }

    final GestureDetector.OnDoubleTapListener listener = new GestureDetector.OnDoubleTapListener() {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public boolean onDoubleTap(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent motionEvent) {
            return false;
        }
    };

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {return arrayListMascotas.size();}

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView image;
        private ImageButton  imbComm;
        private TextView id_mascota;


        private ViewHolder(final View itemView){
            super(itemView);
            this.image = itemView.findViewById(R.id.imageViewLayout);
            this.imbComm = itemView.findViewById(R.id.btnMensaje);
            this.id_mascota = itemView.findViewById(R.id.id_mascota);



            //this.image.setOn

            imbComm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // Toast.makeText(itemView.getContext().getApplicationContext(), "taph", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(view.getContext().getApplicationContext(), Comentarios_publ.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    itemView.getContext().startActivity(i);
                }
            });
            final ImageButton imbt1  = itemView.findViewById(R.id.btnMegusta);
            final ImageButton imbInteres = itemView.findViewById(R.id.btnMeInteresa);

            imbt1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if( imbt1.getTag()=="negro")
                    {
                        ObtenerWebService hiloconexion;
                         String IP="http://guau.000webhostapp.com";
                         String INSERT_MASCOTA_LIKE= IP + "/mascota_inserta_like.php?";
                        Calendar cal = new GregorianCalendar();
                        INSERT_MASCOTA_LIKE = INSERT_MASCOTA_LIKE+"id_usuario=1&id_mascota="+id_mascota.getText()+"&fecha_like="+cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH);
                        hiloconexion = new ObtenerWebService(itemView.getContext());
                        hiloconexion.execute(INSERT_MASCOTA_LIKE, "3");
                        imbt1.setImageResource(R.drawable.ic_favorite_black_24dp);
                        imbt1.setTag("rojo");
                    }
                    else if(imbt1.getTag()=="rojo"){
                        ObtenerWebService hiloconexion;
                        String IP="http://guau.000webhostapp.com";
                        String DELETE_MASCOTA_LIKE= IP + "/mascota_borrar_like.php?";
                        DELETE_MASCOTA_LIKE = DELETE_MASCOTA_LIKE+"id_usuario=1&id_mascota="+id_mascota.getText();
                        hiloconexion = new ObtenerWebService(itemView.getContext());
                        hiloconexion.execute(DELETE_MASCOTA_LIKE, "3");
                        imbt1.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                        imbt1.setTag("negro");
                    }
                }
            });

            imbInteres.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(imbInteres.getTag()=="negro"){
                        ObtenerWebService hiloconexion;
                        String IP="http://guau.000webhostapp.com";
                        String INSERT_MASCOTA_INTERES= IP + "/mascota_inserta_interesado.php?";
                        Calendar cal = new GregorianCalendar();
                        INSERT_MASCOTA_INTERES = INSERT_MASCOTA_INTERES+"id_usuario=1&id_mascota="+id_mascota.getText()+"&fecha_interes="+cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH);
                        hiloconexion = new ObtenerWebService(itemView.getContext());
                        hiloconexion.execute(INSERT_MASCOTA_INTERES, "3");
                        imbInteres.setImageResource(R.drawable.ic_sentiment_very_satisfied_red_24dp);
                        imbInteres.setTag("rojo");
                    }
                    else if(imbInteres.getTag()=="rojo"){
                        ObtenerWebService hiloconexion;
                        String IP="http://guau.000webhostapp.com";
                        String DELETE_MASCOTA_INTERES= IP + "/mascota_borrar_interesado.php?";
                        DELETE_MASCOTA_INTERES = DELETE_MASCOTA_INTERES+"id_usuario=1&id_mascota="+id_mascota.getText();
                        hiloconexion = new ObtenerWebService(itemView.getContext());
                        hiloconexion.execute(DELETE_MASCOTA_INTERES, "3");
                        imbInteres.setImageResource(R.drawable.ic_sentiment_very_satisfied_black_24dp);
                        imbInteres.setTag("negro");
                    }
                }
            });


        }
    }
}
