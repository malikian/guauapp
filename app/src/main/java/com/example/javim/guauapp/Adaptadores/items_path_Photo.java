package com.example.javim.guauapp.Adaptadores;

/**
 * Created by javim on 27/02/2018.
 */

public class items_path_Photo {
    private int id_mascota;
    private String path_photo;

    public int getId_mascota() {
        return id_mascota;
    }

    public void setId_mascota(int id_mascota) {
        this.id_mascota = id_mascota;
    }

    public String getPath_photo() {
        return path_photo;
    }

    public void setPath_photo(String path_photo) {
        this.path_photo = path_photo;
    }

    public items_path_Photo(int id_mascota, String path_photo) {
        this.id_mascota = id_mascota;
        this.path_photo = path_photo;
    }




}
