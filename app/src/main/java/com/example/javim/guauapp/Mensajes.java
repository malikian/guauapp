package com.example.javim.guauapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.javim.guauapp.Adaptadores.Adapter_mensajes;
import com.example.javim.guauapp.Adaptadores.items_mensajes;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Mensajes.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Mensajes#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Mensajes extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ListView listaMensajes=null;
    private ArrayList<items_mensajes> arrayTems_mensajes = null;
    private Adapter_mensajes adapter =null;
    public static Context context;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Mensajes() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Mensajes.
     */
    // TODO: Rename and change types and number of parameters
    public static Mensajes newInstance(String param1, String param2) {
        Mensajes fragment = new Mensajes();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_mensajes, container, false);
        listaMensajes = v.findViewById(R.id.ListViewMensajes);
        arrayTems_mensajes = new ArrayList<>();
        Bitmap bitmap = BitmapFactory.decodeResource(getContext().getResources(),R.drawable.perrodos);
        cargarLista(bitmap);
        return v;
    }

    private void cargarLista(Bitmap bitmap){

        arrayTems_mensajes.add(new items_mensajes("Javier","1","Hola, quiero informes del perrito llamado pulgas, donde lo tienes, me gustaría conocerlo", bitmap));
        arrayTems_mensajes.add(new items_mensajes("Javier2","2","Hola, quiero informes del perrito llamado pulgas, donde lo tienes, me gustaría conocerlo", bitmap));
        arrayTems_mensajes.add(new items_mensajes("Javier3","3","Hola, quiero informes del perrito llamado pulgas, donde lo tienes, me gustaría conocerlo", bitmap));
        arrayTems_mensajes.add(new items_mensajes("Javier4","4","Hola, quiero informes del perrito llamado pulgas, donde lo tienes, me gustaría conocerlo", bitmap));
        arrayTems_mensajes.add(new items_mensajes("Javier5","5","Hola, quiero informes del perrito llamado pulgas, donde lo tienes, me gustaría conocerlo", bitmap));
        arrayTems_mensajes.add(new items_mensajes("Javier6","6","Hola, quiero informes del perrito llamado pulgas, donde lo tienes, me gustaría conocerlo", bitmap));
        arrayTems_mensajes.add(new items_mensajes("Javier7","7","Hola, quiero informes del perrito llamado pulgas, donde lo tienes, me gustaría conocerlo", bitmap));

        adapter = new Adapter_mensajes(arrayTems_mensajes, this.getActivity().getApplicationContext());
        listaMensajes.setAdapter(adapter);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
