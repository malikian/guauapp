package com.example.javim.guauapp.Clases;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by javim on 14/11/2017.
 */

public  class ComprobacionesGenerales {
    private Context Mcontext;

    public ComprobacionesGenerales(Context context2){
        this.Mcontext = context2;
    }

    public boolean conexion(){
        ConnectivityManager cn = (ConnectivityManager)Mcontext.getSystemService(Mcontext.CONNECTIVITY_SERVICE);

        assert cn != null;
        NetworkInfo info_wifi = cn.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo info_datos = cn.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if(String.valueOf(info_wifi.getState()).equals("CONNECTED"))
            return true;
        else {
            if(String.valueOf(info_datos.getState()).equals("CONNECTED"))
                return true;
            else
                return false;

            }
        }

    }

