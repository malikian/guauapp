package com.example.javim.guauapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.javim.guauapp.Adaptadores.AdapterFotosMascotas;
import com.example.javim.guauapp.Adaptadores.Items_fotos_peq;
import com.example.javim.guauapp.Adaptadores.Items_mascotas;
import com.example.javim.guauapp.Adaptadores.Items_usuarios;
import com.example.javim.guauapp.Adaptadores.MascotaAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;


public class perfil_general extends Fragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Button btnVerMascotas;
    private TextView nombreUsuario;
    private TextView mascotasPubl;
    private TextView noTelefono;
    private TextView miembroDesde;
    private  TextView correo;
    private TextView noTelefono2;
    private TextView direccion;
    private TextView mupoEstado;

    private TextView reseniaTv;
    private TextView reseniaTv2;
    private TextView reseniaTv3;

    private AdapterFotosMascotas mascotaAdapter;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private final int PERMISSION_READ_EXTERNAL_MEMORY = 1;
    String IP="http://guau.000webhostapp.com";
    String OBTENER_PATH_MASCOTA= IP + "/mascotas_obt_tod_fotos.php?id_usuario=1";
    String OBTENER_DATOS_CLIENTE=IP + "/obtener_cliente_by_id.php?id_usuario=1";


    ObtenerWebService hiloconexion;
    ObtenerWebService2 hiloconexion2;

    public ArrayList<Items_fotos_peq> arrayTems_mascotas = null;
    public ArrayList<Items_usuarios> arrayTems_Cliente = null;
    ImageView imImagen;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    View v;

    public perfil_general() {

    }

    public static perfil_general newInstance(String param1, String param2) {
        perfil_general fragment = new perfil_general();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v=inflater.inflate(R.layout.fragment_perfil_general, container, false);
        btnVerMascotas = v.findViewById(R.id.btnVermascotas);

        btnVerMascotas.setOnClickListener(this);

        imImagen = v.findViewById(R.id.imageViewLayout);

        arrayTems_mascotas = new ArrayList<>();
        arrayTems_mascotas.clear();
        arrayTems_Cliente= new ArrayList<>();
        arrayTems_Cliente.clear();

        getMascotasLink2();
        getMascotasLink();
       //getMascotasLink3();

       nombreUsuario = v.findViewById(R.id.nombreUsuario);
       mascotasPubl = v.findViewById(R.id.mascotasPubl);
       noTelefono = v.findViewById(R.id.noTelefono);
        noTelefono2 = v.findViewById(R.id.noTelefono2);
        miembroDesde = v.findViewById(R.id.miembroDesde);
        correo = v.findViewById(R.id.correo);
        direccion = v.findViewById(R.id.direccion);
        mupoEstado = v.findViewById(R.id.mupoEstado);

        noTelefono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_DIAL);
                i.setData(Uri.parse("tel:"+noTelefono.getText().toString()));
                startActivity(i);
            }
        });

        mascotaAdapter = new AdapterFotosMascotas(getContext(),arrayTems_mascotas, R.layout.layout_img_small);
        recyclerView = v.findViewById(R.id.recyclerView);
        layoutManager = new GridLayoutManager(getContext(),3);

        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(true);

        recyclerView.setLayoutManager(layoutManager);

        AdView mAdView = v.findViewById(R.id.adViewPG);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        MobileAds.initialize(getContext());

        return v;
    }
    public void getMascotasLink(){
        if (hiloconexion == null){
            hiloconexion = new ObtenerWebService();
        }
        hiloconexion.execute(OBTENER_PATH_MASCOTA, "1");
       // hiloconexion.execute(OBTENER_DATOS_CLIENTE, "2");
    }
    public void getMascotasLink2(){
        if (hiloconexion2 == null){
            hiloconexion2 = new ObtenerWebService2();
        }

         hiloconexion2.execute(OBTENER_DATOS_CLIENTE, "2");
    }

    public void getMascotasLink3() {
        if (hiloconexion2 == null){
            hiloconexion2 = new ObtenerWebService2();
        }

        hiloconexion2.execute(OBTENER_DATOS_CLIENTE, "2");
    }
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        if(view == btnVerMascotas){
            Intent i = new Intent(getActivity(), image_big.class);
            startActivity(i);
        }
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    //web service
    public class ObtenerWebService extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(getContext(), "Cargando, espere...", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show();
            recyclerView.setAdapter(mascotaAdapter);

            //mascotaAdapter = new arrayTems_mascotas( arrayTems_mascotas,getApplicationContext());
            // ListaPersonalizada.setAdapter(mascotaAdapter);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }


        @Override
        protected String doInBackground(String... strings) {
            String cadena =strings[0];
            URL url=null;
            String devuelve="";
            String datosConsulta="";
            if(Objects.equals(strings[1], "1")){
                try{
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if(respuesta== HttpURLConnection.HTTP_OK){//200 ok 403 para no conexin
                        InputStream in = new BufferedInputStream(connection.getInputStream());
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                        String line;
                        while ((line = reader.readLine())!= null){
                            result.append(line);
                        }

                        JSONObject respuestaJSON = new JSONObject(result.toString());
                        // JSONObject respuestaJSON = new JSONObject(result.toString());
                        String resultJSON = respuestaJSON.getString("status");

                        if(resultJSON.equals("1")){
                            JSONArray consultaJSON = respuestaJSON.getJSONArray("JsonResults");

                             for (int i=0;i < consultaJSON.length();i++){
                            arrayTems_mascotas.add(new Items_fotos_peq(
                                    consultaJSON.getJSONObject(i).getString("id_mascota"),
                                    consultaJSON.getJSONObject(i).getString("path_foto")));
                            }
                        }
                        devuelve = respuestaJSON.getString("status");;
                    }
                    else
                        devuelve = "Revisa la conexion a internet e int?ntalo nuevamente";

                    connection.disconnect();

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    devuelve = e.getMessage();
                }

            }
            return devuelve;
        }
    }
    //web service
    public class ObtenerWebService2 extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Toast.makeText(getContext(), "Cargando, espere...", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show();
            recyclerView.setAdapter(mascotaAdapter);
            nombreUsuario.setText(arrayTems_Cliente.get(0).getNombreUsuario()+" "+arrayTems_Cliente.get(0).getApelsUsuario());
            mascotasPubl.setText(arrayTems_Cliente.get(0).getMascotasPubl()+" ");
            noTelefono.setText(arrayTems_Cliente.get(0).getNoTelefono()+" ");
            miembroDesde.setText("Miembro de Guau desde "+arrayTems_Cliente.get(0).getMiembroDesde());
            correo.setText(arrayTems_Cliente.get(0).getCorreo());
            noTelefono2.setText(arrayTems_Cliente.get(0).getNoTelefono()+" ");
            direccion.setText(arrayTems_Cliente.get(0).getDireccion());
            mupoEstado.setText(arrayTems_Cliente.get(0).getMupoEstado());

            //mascotaAdapter = new arrayTems_mascotas( arrayTems_mascotas,getApplicationContext());
            // ListaPersonalizada.setAdapter(mascotaAdapter);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }


        @Override
        protected String doInBackground(String... strings) {
            String cadena =strings[0];
            URL url=null;
            String devuelve="";
            String datosConsulta="";

            if(Objects.equals(strings[1], "2")){
                try{
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");


                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if(respuesta== HttpURLConnection.HTTP_OK){//200 ok 403 para no conexin
                        InputStream in = new BufferedInputStream(connection.getInputStream());
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                        String line;
                        while ((line = reader.readLine())!= null){
                            result.append(line);
                        }

                        JSONObject respuestaJSON = new JSONObject(result.toString());
                        // JSONObject respuestaJSON = new JSONObject(result.toString());
                        String resultJSON = respuestaJSON.getString("status");

                        if(resultJSON.equals("1")){
                            JSONObject consultaJSON = respuestaJSON.getJSONObject("JsonResults");

                            arrayTems_Cliente.add(new Items_usuarios(
                                    consultaJSON.getString("nombre_usuario"),
                                    consultaJSON.getString("apels_usuario"),
                                    consultaJSON.getInt("mascotasPubl"),
                                    consultaJSON.getString("telefono"),
                                    consultaJSON.getString("email"),
                                    consultaJSON.getString("direccion"),
                                    consultaJSON.getString("mupoEstado"),
                                    consultaJSON.getString("fecha_alta")));
                        }
                        devuelve = respuestaJSON.getString("status");;
                    }
                    else
                        devuelve = "Revisa la conexion a internet e int?ntalo nuevamente";

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    devuelve = e.getMessage();
                }
            }
            return devuelve;
        }
    }

}
