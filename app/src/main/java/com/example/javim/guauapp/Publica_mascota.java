package com.example.javim.guauapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Publica_mascota.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Publica_mascota#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Publica_mascota extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Publica_mascota() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Publica_mascota.
     */
    // TODO: Rename and change types and number of parameters
    public static Publica_mascota newInstance(String param1, String param2) {
        Publica_mascota fragment = new Publica_mascota();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_publica_mascota, container, false);
        final FragmentManager frMng = getActivity().getSupportFragmentManager();
        Spinner spinner =  v.findViewById(R.id.AniosMeses);
        String[] letra = {"Meses/años","Meses","Años"};
        spinner.setAdapter(new ArrayAdapter<String>(this.getContext(), R.layout.spinner_items, letra));

        Spinner spinner2 =  v.findViewById(R.id.tamanio);
        String[] tmano = {"Tamaño","Chico","Mediano", "Grande"};
        spinner2.setAdapter(new ArrayAdapter<String>(this.getContext(), R.layout.spinner_items, tmano));

        Button btnSiguiente = v.findViewById(R.id.siguente);
        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frMng.beginTransaction().replace(R.id.content, new Publica2_mascota()).commit();
            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
