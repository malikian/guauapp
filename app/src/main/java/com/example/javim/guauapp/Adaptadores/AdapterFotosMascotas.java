package com.example.javim.guauapp.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.javim.guauapp.R;
import com.example.javim.guauapp.image_big;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;


public class AdapterFotosMascotas extends RecyclerView.Adapter<AdapterFotosMascotas.ViewHolder>
{
    private Context context;
    private int layout;
    private ArrayList<Items_fotos_peq> arrayListMascotas;

    private TextView id_mascota;
    private ImageView imageViewLayoutSmall;

    public  AdapterFotosMascotas(Context context, ArrayList<Items_fotos_peq> arrayListMascotas, int layout)
    {
        this.context = context;
        this.arrayListMascotas = arrayListMascotas;
        this.layout = layout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(context).inflate(layout, null, false);
        id_mascota = v.findViewById(R.id.id_mascota);
        imageViewLayoutSmall = v.findViewById(R.id.imageViewLayoutSmall);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Picasso.with(context).load(arrayListMascotas.get(position).getPath_photo()).fit().placeholder(R.drawable.ic_camera_black_24dp).into(holder.image, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {}
            @Override
            public void onError() {
                //Toast.makeText(context, "Error! Revisa tu conexion a internet y vuelve a intentarlo", Toast.LENGTH_LONG).show();
            }
        });
        imageViewLayoutSmall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, image_big.class);
                i.putExtra("id_mascota",arrayListMascotas.get(position).getId_mascota());
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {return arrayListMascotas.size();}

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView image;
        private ImageButton imbComm;
        private TextView id_mascota;


        private ViewHolder(final View itemView){
            super(itemView);
            this.image = itemView.findViewById(R.id.imageViewLayoutSmall);
            this.id_mascota = itemView.findViewById(R.id.id_mascota);
        }
    }

}

