package com.example.javim.guauapp.Adaptadores;

import android.graphics.Bitmap;

/**
 * Created by javim on 01/11/2017.
 */

public class items_mensajes {


    private String nombre_persona;
    private String id;
    private String Mensaje;
    private Bitmap foto;

    public items_mensajes(String nombre_persona, String id, String mensaje, Bitmap foto) {
        this.nombre_persona = nombre_persona;
        this.id = id;
        this.Mensaje = mensaje;
        this.foto = foto;
    }

    public String getNombre_persona() {
        return nombre_persona;
    }

    public void setNombre_persona(String nombre_persona) {
        this.nombre_persona = nombre_persona;
    }

    public String getId() {return id;}

    public void setId(String id) {this.id = id;}

    public String getMensaje() {return Mensaje;}

    public void setMensaje(String mensaje) {Mensaje = mensaje;}

    public Bitmap getFoto() {return foto;}

    public void setFoto(Bitmap foto) {this.foto = foto;}




}
