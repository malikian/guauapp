package com.example.javim.guauapp.Clases;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.example.javim.guauapp.AcPublica_mascota;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by javim on 14/11/2017.
 */

public class ObtenerWebService extends AsyncTask<String, Void, String> {

    @SuppressLint("StaticFieldLeak")
    private Context mContext;

    public ObtenerWebService(Context context){
        this.mContext = context;
    }



    @Override
    protected String doInBackground(String... strings) {
        String cadena=strings[0];
        URL url = null;
        String devuelve = "";
        switch (strings[1]){
            case "1":
                try{
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                            " (Linux: Android 1.5; es-ES) Ejemplo HTTP");
                    //connection.setRequestProperty("Content-Type", "aplication/json");
                    connection.setConnectTimeout(7000);
                    connection.setReadTimeout(7000);
                    connection.connect();

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if(respuesta==HttpURLConnection.HTTP_OK){
                        InputStream in = new BufferedInputStream(connection.getInputStream());//prepara la cadena de entrada
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));//la introduzco en un buffer
                        String line;
                        while((line=reader.readLine())!= null){
                            result.append(line);
                        }
                        //Se crea el objeto JSON para poder acceder a los atributos del objeto
                        JSONObject respuestaJSON= new JSONObject(result.toString());
                        //Accedemos al vector de resultados
                        String resultJSON = respuestaJSON.getString("estado");

                        if(resultJSON.equals("1")){
                            //JSONArray clientesJSON = respuestaJSON.getJSONArray("mensaje");
                            devuelve = respuestaJSON.getString("mensaje");
                        }
                        else{
                            devuelve = "Error al insertar.";
                        }
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    devuelve = "Error: "+e.getMessage();

                }
                break;
            case "2":
                try{
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                            " (Linux: Android 1.5; es-ES) Ejemplo HTTP");
                    //connection.setRequestProperty("Content-Type", "aplication/json");
                    connection.setRequestMethod("GET");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if(respuesta==HttpURLConnection.HTTP_OK){
                        InputStream in = new BufferedInputStream(connection.getInputStream());//prepara la cadena de entrada
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));//la introduzco en un buffer
                        String line;
                        while((line=reader.readLine())!= null){
                            result.append(line);
                        }
                        //Se crea el objeto JSON para poder acceder a los atributos del objeto
                        JSONObject respuestaJSON= new JSONObject(result.toString());
                        //Accedemos al vector de resultados
                        String resultJSON = respuestaJSON.getString("estado");
                        //String direccion = "Sin datos de Cliente";
                        if(resultJSON.equals("1")){
                            //JSONArray clientesJSON = respuestaJSON.getJSONArray("mensaje");
                            devuelve = respuestaJSON.getString("mensaje");
                        }
                        else{
                            devuelve = "Error al insertar.";
                        }
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "Error: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
            case "3":
                try{
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                            " (Linux: Android 1.5; es-ES) Ejemplo HTTP");
                    //connection.setRequestProperty("Content-Type", "aplication/json");
                    connection.setRequestMethod("GET");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if(respuesta==HttpURLConnection.HTTP_OK){
                        InputStream in = new BufferedInputStream(connection.getInputStream());//prepara la cadena de entrada
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));//la introduzco en un buffer
                        String line;
                        while((line=reader.readLine())!= null){
                            result.append(line);
                        }
                        //Se crea el objeto JSON para poder acceder a los atributos del objeto
                        JSONObject respuestaJSON= new JSONObject(result.toString());
                        //Accedemos al vector de resultados
                        String resultJSON = respuestaJSON.getString("status");
                        //String direccion = "Sin datos de Cliente";
                        if(resultJSON.equals("1")){
                            //JSONArray clientesJSON = respuestaJSON.getJSONArray("mensaje");
                            devuelve = respuestaJSON.getString("message");
                        }
                        else{
                            devuelve = "Error al insertar."+respuestaJSON.getString("message");
                        }
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                   // Toast.makeText(mContext, "Error: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;

        }
        return devuelve;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        Toast.makeText(mContext,"Es: "+s, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        //super.onProgressUpdate(values);
        ProgressDialog progress = new ProgressDialog(mContext);
        progress.setMessage("Guardando...");
        progress.show();
    }


}
