package com.example.javim.guauapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.javim.guauapp.Adaptadores.CommentsAdapter;
import com.example.javim.guauapp.Adaptadores.Items_comments;
import com.example.javim.guauapp.Adaptadores.Items_mascotas;

import java.util.ArrayList;

public class Comentarios_publ extends AppCompatActivity {

    private CommentsAdapter commentsAdapter;
    private RecyclerView recyclerView_Comments;
    private RecyclerView.LayoutManager layoutManager_Comments;
    public ArrayList<Items_comments> arrayTems_comments = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentarios_publ);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        arrayTems_comments = new ArrayList<>();
        arrayTems_comments.clear();
        getCommentsLink();
        commentsAdapter = new CommentsAdapter(this,arrayTems_comments, R.layout.comentarios_layout);
        recyclerView_Comments = (RecyclerView) findViewById(R.id.recyclerView_comentarios);

        layoutManager_Comments = new GridLayoutManager(this,1);


        recyclerView_Comments.setHasFixedSize(true);
        recyclerView_Comments.setNestedScrollingEnabled(true);

        recyclerView_Comments.setLayoutManager(layoutManager_Comments);

        recyclerView_Comments.setAdapter(commentsAdapter);

    }

    public void getCommentsLink(){
        //int id_comentario, int id_usuario, int id_mascota, String comentarios, String fecha_comentario, String nombreUsuario
        //hiloconexion = new Galeria_principal.ObtenerWebService();
        //hiloconexion.execute(INSERT_MASCOTA, "1");

       arrayTems_comments.add(new Items_comments(1, 1, 1,"Comentario  de una persona que comento en el area de comentacion", "05/feb/2018", "Javier Castro"));
        arrayTems_comments.add(new Items_comments(2, 1, 1,"Comentario 2", "05/feb/2018", "Javier Castro"));
        arrayTems_comments.add(new Items_comments(3, 1, 1,"Comentario 3", "05/feb/2018", "Javier Castro"));
        arrayTems_comments.add(new Items_comments(4, 1, 1,"Comentario 4", "05/feb/2018", "Javier Castro"));
        arrayTems_comments.add(new Items_comments(5, 1, 1,"Comentario 5", "05/feb/2018", "Javier Castro"));



    }
}
