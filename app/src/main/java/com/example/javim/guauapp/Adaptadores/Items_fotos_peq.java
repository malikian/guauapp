package com.example.javim.guauapp.Adaptadores;

/**
 * Created by javim on 07/03/2018.
 */

public class Items_fotos_peq {
    public String getId_mascota() {
        return id_mascota;
    }

    public void setId_mascota(String id_mascota) {
        this.id_mascota = id_mascota;
    }

    public String getPath_photo() {
        return Path_photo;
    }

    public void setPath_photo(String path_photo) {
        Path_photo = path_photo;
    }

    public Items_fotos_peq(String id_mascota, String path_photo) {
        this.id_mascota = id_mascota;
        Path_photo = path_photo;
    }

    private String id_mascota;
    private String Path_photo;


}
