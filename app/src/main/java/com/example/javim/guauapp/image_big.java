package com.example.javim.guauapp;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.javim.guauapp.Adaptadores.Items_mascotas;
import com.example.javim.guauapp.Adaptadores.MascotaAdapter;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

public class image_big extends AppCompatActivity {

    //variables globales
    public MascotaAdapter mascotaAdapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private final int PERMISSION_READ_EXTERNAL_MEMORY = 1;
    String IP="http://guau.000webhostapp.com";
    String OBTENER_MASCOTA;
    ObtenerWebService hiloconexion;
    public ArrayList<Items_mascotas> arrayTems_mascotas = null;
    ImageView imImagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_big);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        OBTENER_MASCOTA= IP + "/mascotas_getBy_id.php?id_mascota="+ Objects.requireNonNull(getIntent().getExtras()).getString("id_mascota")+"&id_cliente=1";

        imImagen = (ImageView) findViewById(R.id.imageViewLayout);
        arrayTems_mascotas = new ArrayList<>();
        arrayTems_mascotas.clear();

        getMascotasLink();

        mascotaAdapter = new MascotaAdapter(this,arrayTems_mascotas, R.layout.image_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewBig);
        layoutManager = new GridLayoutManager(this,1);

        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mascotaAdapter);
    }
    public void getMascotasLink(){
        hiloconexion = new ObtenerWebService();
        hiloconexion.execute(OBTENER_MASCOTA, "1");
    }
    //web service--------------------------------------------------------
    public class ObtenerWebService extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            Toast.makeText(getApplicationContext(), "Cargando, espere...", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
            recyclerView.setAdapter(mascotaAdapter);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }


        @Override
        protected String doInBackground(String... strings) {
            String cadena =strings[0];
            URL url=null;
            String devuelve="";
            String datosConsulta="";
            if(Objects.equals(strings[1], "1")){
                try{
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");


                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if(respuesta== HttpURLConnection.HTTP_OK){//200 ok 403 para no conexin
                        InputStream in = new BufferedInputStream(connection.getInputStream());
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                        String line;
                        while ((line = reader.readLine())!= null){
                            result.append(line);
                        }

                        JSONObject respuestaJSON = new JSONObject(result.toString());

                        String resultJSON = respuestaJSON.getString("status");

                        if(resultJSON.equals("1")){
                            JSONObject consultaJSON = respuestaJSON.getJSONObject("JsonResults");
                                arrayTems_mascotas.add(new Items_mascotas(
                                        consultaJSON.getInt("id_mascota"),
                                        consultaJSON.getInt("id_usuario"),
                                        consultaJSON.getString("nombre"),
                                        consultaJSON.getString("especie"),
                                        consultaJSON.getString("raza"),
                                        consultaJSON.getInt("edad"),
                                        consultaJSON.getInt("mes_anio"),
                                        consultaJSON.getString("descripcion"),
                                        consultaJSON.getInt("likes"),
                                        consultaJSON.getInt("interesados"),
                                        consultaJSON.getString("tamanio"),
                                        consultaJSON.getString("fecha_public"),
                                        1,
                                        consultaJSON.getString("path_foto"),
                                        consultaJSON.getInt("likesM"),
                                        consultaJSON.getInt("interes"),
                                        consultaJSON.getString("nombre_usuario"),
                                        consultaJSON.getString("telefono")));
                        }
                        devuelve = respuestaJSON.getString("status");;
                    }
                    else
                        devuelve = "Revisa la conexion a internet e intÚntalo nuevamente";
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    devuelve = e.getMessage();
                }
            }
            return devuelve;
        }
    }
}
