package com.example.javim.guauapp;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.example.javim.guauapp.Adaptadores.Items_mascotas;
import com.example.javim.guauapp.Adaptadores.MascotaAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;




public class Galeria_principal extends AppCompatActivity {

    private MascotaAdapter mascotaAdapter;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private final int PERMISSION_READ_EXTERNAL_MEMORY = 1;
    String IP="http://guau.000webhostapp.com";
    String INSERT_MASCOTA= IP + "/obtener_mascotas.php?id_usuario=1";
    ObtenerWebService hiloconexion;
    public ArrayList<Items_mascotas> arrayTems_mascotas = null;
    GestureDetector gestureDetector;
    ImageView imImagen;
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galeria_principal);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final ImageButton imbComm = (ImageButton) findViewById(R.id.btnMensaje);
        imImagen = (ImageView) findViewById(R.id.imageViewLayout);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        arrayTems_mascotas = new ArrayList<>();
        arrayTems_mascotas.clear();
        getMascotasLink();
        mascotaAdapter = new MascotaAdapter(this,arrayTems_mascotas, R.layout.image_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        layoutManager = new GridLayoutManager(this,1);

        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(true);

        recyclerView.setLayoutManager(layoutManager);




        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        MobileAds.initialize(this);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }
        });


    }

   public void getMascotasLink(){
       hiloconexion = new ObtenerWebService();
       hiloconexion.execute(INSERT_MASCOTA, "1");
   }

    //web service--------------------------------------------------------
   public class ObtenerWebService extends AsyncTask<String, Void, String>{
       @Override
       protected void onPreExecute() {
           super.onPreExecute();
           progressBar.setVisibility(View.VISIBLE);
       }

       @Override
       protected void onPostExecute(String s) {
           super.onPostExecute(s);
           progressBar.setVisibility(View.GONE);
           if(s.equals("1"))
            recyclerView.setAdapter(mascotaAdapter);
           else
               Toast.makeText(getApplicationContext(),""+s, Toast.LENGTH_SHORT).show();
       }

       @Override
       protected void onProgressUpdate(Void... values) {
           super.onProgressUpdate(values);
       }

       @Override
       protected void onCancelled() {
           super.onCancelled();
       }

       @Override
       protected String doInBackground(String... strings) {
           String cadena =strings[0];
           URL url=null;
           String devuelve="";
           if(Objects.equals(strings[1], "1")){
               try{
                   url = new URL(cadena);
                   HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                   connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                           "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                   int respuesta = connection.getResponseCode();
                   StringBuilder result = new StringBuilder();

                   if(respuesta== HttpURLConnection.HTTP_OK){//200 ok 403 para no conexin
                       InputStream in = new BufferedInputStream(connection.getInputStream());
                       BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                       String line;
                       while ((line = reader.readLine())!= null){
                           result.append(line);
                       }

                       JSONObject respuestaJSON = new JSONObject(result.toString());
                       String resultJSON = respuestaJSON.getString("estado");

                       if(resultJSON.equals("1")){
                           JSONArray consultaJSON = respuestaJSON.getJSONArray("mascotas");

                           for (int i=0;i < consultaJSON.length();i++){
                               arrayTems_mascotas.add(new Items_mascotas(
                                       consultaJSON.getJSONObject(i).getInt("id_mascota"),
                                       consultaJSON.getJSONObject(i).getInt("id_usuario"),
                                       consultaJSON.getJSONObject(i).getString("nombre"),
                                       consultaJSON.getJSONObject(i).getString("especie"),
                                       consultaJSON.getJSONObject(i).getString("raza"),
                                       consultaJSON.getJSONObject(i).getInt("edad"),
                                       consultaJSON.getJSONObject(i).getInt("mes_anio"),
                                       consultaJSON.getJSONObject(i).getString("descripcion"),
                                       consultaJSON.getJSONObject(i).getInt("likes"),
                                       consultaJSON.getJSONObject(i).getInt("interesados"),
                                       consultaJSON.getJSONObject(i).getString("tamanio"),
                                       consultaJSON.getJSONObject(i).getString("fecha_public"),
                                       1,
                                       consultaJSON.getJSONObject(i).getString("path_foto"),
                                       consultaJSON.getJSONObject(i).getInt("likesM"),
                                       consultaJSON.getJSONObject(i).getInt("interes"),
                                       consultaJSON.getJSONObject(i).getString("nombre_usuario"),
                                       consultaJSON.getJSONObject(i).getString("telefono")));
                           }
                       }
                       devuelve = respuestaJSON.getString("estado");
                   }
                   else
                       devuelve = "Revisa la conexion a internet e inténtalo nuevamente";
               } catch (IOException | JSONException e) {
                   e.printStackTrace();
                   devuelve = e.getMessage();
               }
           }
           return devuelve;
       }
   }
}




