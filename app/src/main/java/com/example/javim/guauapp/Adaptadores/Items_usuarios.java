package com.example.javim.guauapp.Adaptadores;

/**
 * Created by javim on 06/03/2018.
 */

public class Items_usuarios {
    private String nombreUsuario;
    private String apelsUsuario;
    private int mascotasPubl;
    private String noTelefono;
    private String correo;
    private String direccion;
    private String mupoEstado;
    private String miembroDesde;

    public Items_usuarios(String nombreUsuario, String apelsUsuario, int mascotasPubl, String noTelefono, String correo, String direccion, String mupoEstado,String miembroDesde) {
        this.nombreUsuario = nombreUsuario;
        this.apelsUsuario = apelsUsuario;
        this.mascotasPubl = mascotasPubl;
        this.noTelefono = noTelefono;
        this.correo = correo;
        this.direccion = direccion;
        this.mupoEstado = mupoEstado;
        this.miembroDesde = miembroDesde;
    }

    public String getMiembroDesde() {return miembroDesde;}

    public void setMiembroDesde(String miembroDesde) {this.miembroDesde = miembroDesde;}

    public String getNombreUsuario() {return nombreUsuario;}

    public void setNombreUsuario(String nombreUsuario) {this.nombreUsuario = nombreUsuario;}

    public String getApelsUsuario() {return apelsUsuario;}

    public void setApelsUsuario(String apelsUsuario) {this.apelsUsuario = apelsUsuario;}

    public int getMascotasPubl() {return mascotasPubl;}

    public void setMascotasPubl(int mascotasPubl) {this.mascotasPubl = mascotasPubl;}

    public String getNoTelefono() {return noTelefono;}

    public void setNoTelefono(String noTelefono) {this.noTelefono = noTelefono;}

    public String getCorreo() {return correo;}

    public void setCorreo(String correo) {this.correo = correo;}

    public String getDireccion() {return direccion;}

    public void setDireccion(String direccion) {this.direccion = direccion;}

    public String getMupoEstado() {return mupoEstado;}

    public void setMupoEstado(String mupoEstado) {this.mupoEstado = mupoEstado;}
}
