package com.example.javim.guauapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.javim.guauapp.Clases.ComprobacionesGenerales;

public class MainActivity extends AppCompatActivity implements
        Peludos.OnFragmentInteractionListener, Busqueda.OnFragmentInteractionListener,
        Publica_mascota.OnFragmentInteractionListener, Publica2_mascota.OnFragmentInteractionListener,
        Mensajes.OnFragmentInteractionListener, perfil_general.OnFragmentInteractionListener
       {
    private TextView mTextMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ComprobacionesGenerales cx = new ComprobacionesGenerales(this);
       /* if(cx.conexion())
            Toast.makeText(this, "Si hay", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, "No hay", Toast.LENGTH_LONG).show();*/

        //hola
        final FragmentManager frMng = getSupportFragmentManager();

        //Toolbar toolbarw = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbarw);

         BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
                = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.mispeludos://mis peludos

                        //frMng.beginTransaction().replace(R.id.content, new Peludos()).commit();
                        final Intent i1 = new Intent(MainActivity.this, Galeria_principal.class);
                        startActivity(i1);
                        //startActivity(i);
                        return true;
                    case R.id.buscaPeludos:
                         frMng.beginTransaction().replace(R.id.content, new Busqueda()).commit();
                        return true;
                    case R.id.publicaPeludos:
                        frMng.beginTransaction().replace(R.id.content, new Peludos()).commit();
                        final Intent i = new Intent(MainActivity.this, AcPublica_mascota.class);
                        startActivity(i);
                        return true;
                    case R.id.mensajePeludos:
                        frMng.beginTransaction().replace(R.id.content,new Mensajes()).commit();
                        return true;
                    case R.id.perfilP:
                        frMng.beginTransaction().replace(R.id.content, new perfil_general()).commit();
                        return true;
                }
                return false;
            }

        };

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        frMng.beginTransaction().replace(R.id.content, new Peludos()).commit();
    }

           @Override
           public void onFragmentInteraction(Uri uri) {

           }



           /*boolean onCreateOptionMenu(Menu menu){
               getMenuInflater().inflate(R.menu.menutool, menu);
               return true;
           }
           @SuppressWarnings("StatementWithEmptyBody")
           @Override
           public boolean onOptionsItemSelected(MenuItem item) {
               int id = item.getItemId();
               if (id == R.id.action_settings) {
                   Uri uri = Uri.parse("http://www.bishop-e.com");
                   Intent inte =new Intent(Intent.ACTION_VIEW,uri);
                   startActivity(inte);

                   return true;
               }
               return super.onOptionsItemSelected(item);
           }*/


         }
