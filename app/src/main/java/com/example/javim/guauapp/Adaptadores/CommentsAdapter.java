package com.example.javim.guauapp.Adaptadores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.javim.guauapp.R;

import java.util.ArrayList;

/**
 * Created by javim on 08/02/2018.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {

    private Context context;
    private int layout;
    private ArrayList<Items_comments> arrayListComments;

    private TextView id_usuario1;
    private TextView nombreUsuario;
    private TextView Comentario;

    public CommentsAdapter (Context context, ArrayList<Items_comments> arrayListComments, int layout){
        this.context = context;
        this.arrayListComments = arrayListComments;
        this.layout = layout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View vi = LayoutInflater.from(context).inflate(layout, null, false);

        id_usuario1 =  vi.findViewById(R.id.idUsuariop);
        nombreUsuario = vi.findViewById(R.id.nombreUsuario);
        Comentario = vi.findViewById(R.id.comentario);

        return new ViewHolder(vi);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
       id_usuario1.setText(arrayListComments.get(position).getId_usuario()+" ");
        nombreUsuario.setText(arrayListComments.get(position).getNombreUsuario());
        Comentario.setText(arrayListComments.get(position).getComentarios());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return arrayListComments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(final View itemView) {
            super(itemView);


        }
    }
}
