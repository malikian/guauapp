package com.example.javim.guauapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.javim.guauapp.Clases.ComprobacionesGenerales;
import com.example.javim.guauapp.Clases.ObtenerWebService;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;

public class AcPublica_mascota extends AppCompatActivity {

     String nombre="";
     String especie="";
     String raza="";
     String descripcion="";
     String tamano="";
     String aniosmeses="";
     String edad="";

     String IP="http://guau.000webhostapp.com";
     String INSERT_MASCOTA= IP + "/inserta_mascota.php?";
    ObtenerWebService hiloconexion;
    int vacunas=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ac_publica_mascota);
        //flecha atrás
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Intent para activity de agregar nueva mascota
        final Intent i = new Intent(AcPublica_mascota.this, Publica_mascotaFotos.class);
        //Llenado de Spinners
        final Spinner spinner = (Spinner) findViewById(R.id.AniosMesesMascota);
        String[] letra = {"Meses/años","Meses","Años"};
        spinner.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_items, letra));

       final Spinner spinner2 = (Spinner) findViewById(R.id.tamanioMascota);
        String[] tmano = {"Tamaño","Chico","Mediano", "Grande"};
        spinner2.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_items, tmano));

        final AutoCompleteTextView tvnombreMascota = (AutoCompleteTextView)findViewById(R.id.nombreMascota);
        final AutoCompleteTextView tvespecieMascota = (AutoCompleteTextView)findViewById(R.id.especieMascota);
        final AutoCompleteTextView tvrazaMascota = (AutoCompleteTextView)findViewById(R.id.razaMascota);
        final AutoCompleteTextView tvEdad = (AutoCompleteTextView)findViewById(R.id.edad);
        final CheckBox cbvacunas = (CheckBox)findViewById(R.id.vacunas);
        final EditText etdescripcionMascota = (EditText)findViewById(R.id.descripcionMascota);



        //ComprobacionesGenerales comprobacionesGenerales = new ComprobacionesGenerales(this);
        //if(comprobacionesGenerales.conexion())
          //  Toast.makeText(this, "", Toast.LENGTH_SHORT).show();


        Button btnNxt = (Button)findViewById(R.id.siguente);
        btnNxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(tvnombreMascota.length() <= 0 ){
                    Toast.makeText(getApplicationContext(), "El campo Nombre debe ser llenado", Toast.LENGTH_SHORT).show();
                    tvnombreMascota.requestFocus();
                }
                else if(tvespecieMascota.length()<= 0 ){
                    Toast.makeText(getApplicationContext(), "El campo Especie debe ser llenado", Toast.LENGTH_SHORT).show();
                    tvespecieMascota.requestFocus();
                }
                else if(tvrazaMascota.length()<= 0 ){
                    Toast.makeText(getApplicationContext(), "El campo Raza debe ser llenado", Toast.LENGTH_SHORT).show();
                    tvrazaMascota.requestFocus();
                }
                else if(spinner.getSelectedItemPosition()==0){
                    Toast.makeText(getApplicationContext(), "Debes seleccionar años o meses", Toast.LENGTH_SHORT).show();
                    spinner.requestFocus();
                }
                else if(spinner2.getSelectedItemPosition()==0){
                    Toast.makeText(getApplicationContext(), "Debes seleccionar el tamaño de tu mascota", Toast.LENGTH_SHORT).show();
                    spinner2.requestFocus();
                }
                else if(etdescripcionMascota.length()<= 0 ){
                    Toast.makeText(getApplicationContext(), "El campo Descripcion debe ser llenado", Toast.LENGTH_SHORT).show();
                    etdescripcionMascota.requestFocus();
                }
                else{
                    Calendar cal = new GregorianCalendar();
                    //String nombre, especie, raza, descripción, tamano, aniosmeses;
                    if(cbvacunas.isChecked())
                        vacunas=1;



                    nombre = tvnombreMascota.getText().toString();
                    especie = tvespecieMascota.getText().toString();
                    raza = tvrazaMascota.getText().toString();
                    edad = tvEdad.getText().toString();
                    descripcion = etdescripcionMascota.getText().toString();
                    tamano = spinner2.getSelectedItem().toString();
                    aniosmeses = spinner.getSelectedItem().toString();
                    //Toast.makeText(getApplicationContext(), "Si se armó", Toast.LENGTH_SHORT).show();
                    if(Objects.equals(tamano, "Chico"))
                        tamano="1";
                    else if(Objects.equals(tamano, "Mediano"))
                        tamano = "2";
                    else if(Objects.equals(tamano, "Grande"))
                        tamano="3";


                    INSERT_MASCOTA = INSERT_MASCOTA + "nombre="+nombre+"&especie="+especie+"&raza="
                            +raza+"&edad="+edad+"&descripcion="+descripcion+"&tamano="
                            +tamano+"&mes_anio="+aniosmeses+"&id_usuario=1&fecha_alta="
                            +cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH)
                            +"&vacunas="+vacunas;

                    hiloconexion = new ObtenerWebService(AcPublica_mascota.this);
                    hiloconexion.execute(INSERT_MASCOTA, "1");

                   startActivity(i);
                }
                //startActivity(i);

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AlertDialog.Builder ADial = new AlertDialog.Builder(this);

        ADial.setTitle("Atención!");
        ADial.setMessage("Está seguro de que quiere cancelar la publicación?");
        ADial.setCancelable(false);
        ADial.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //borrar
            }
        });
        ADial.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface ADial, int id) {

            }
        });
        ADial.show();
        /*switch (position) {
            case 0:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, new Acerca()).addToBackStack(null)
                        .commit();
                break;
            case 2:*/
    }

    /*   public  class ObtenerWebService extends AsyncTask<String, Void, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            Toast.makeText(AcPublica_mascota.this, "Guardando datos...", Toast.LENGTH_SHORT).show();
            super.onProgressUpdate(values);

        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }




        @Override
        protected String doInBackground(String... strings) {
            String cadena=strings[0];
            URL url = null;
            String devuelve;
            switch (strings[1]){
                case "1":
                    try{
                        url = new URL(cadena);
                        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                        connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                        " (Linux: Android 1.5; es-ES) Ejemplo HTTP");

                        int respuesta = connection.getResponseCode();
                        StringBuilder result = new StringBuilder();

                        if(respuesta==HttpURLConnection.HTTP_OK){
                            InputStream in = new BufferedInputStream(connection.getInputStream());//prepara la cadena de entrada
                            BufferedReader reader = new BufferedReader(new InputStreamReader(in));//la introduzco en un buffer
                            String line;
                            while((line=reader.readLine())!= null){
                                result.append(line);
                            }
                            //Se crea el objeto JSON para poder acceder a los atributos del objeto
                            JSONObject respuestaJSON= new JSONObject(result.toString());
                            //Accedemos al vector de resultados
                            String resultJSON = respuestaJSON.getString("estado");
                            //String direccion = "Sin datos de Cliente";
                           // Toast.makeText(AcPublica_mascota.this, ""+resultJSON, Toast.LENGTH_SHORT).show();
                            if(resultJSON.equals("1")){
                              //JSONArray clientesJSON = respuestaJSON.getJSONArray("mensaje");
                                TextView tvRes = (TextView)findViewById(R.id.res);
                                tvRes.setText("Res"+respuestaJSON.getString("mensaje"));
                            }
                        }
                    }
                    catch (MalformedURLException e){
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }


            return null;
        }
    }*/
}
